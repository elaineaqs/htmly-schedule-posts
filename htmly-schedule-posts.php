<?php

# Auto-schedule blog posts

# Choose directories
$dir1 = 'content/USERNAME/blog/CATEGORY/draft';
$dir2 = 'content/USERNAME/blog/CATEGORY/post';

# Get current date
date_default_timezone_set('Asia/Manila');
$current = date('Y-m-d');

# Scan drafts folder excluding . and ..
$drafts = array_diff(scandir($dir1), array('..', '.'));

# Show all drafts
foreach ($drafts as $draft) {

  # Find drafts similar to current date
  if (substr($draft, 0, 10)==$current) {

    # Copy drafts to posts folder
    copy ($dir1."/".$draft, $dir2."/".$draft);
  }

}

?>