# HTMLy Schedule Posts

A simple script to schedule blog posts in HTMLy, a flat file CMS by @danpros (danpros.com), and publish automatically through cron.

# More information

Please find the accompanying blog post for this script [here](https://erca.space/day-25-auto-schedule-htmly-blog-posts/).